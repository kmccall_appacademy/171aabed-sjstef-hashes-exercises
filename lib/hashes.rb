# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
    counts = Hash.new(0)
    str.split.uniq.each{ |word| counts[word.downcase] += word.length }
    counts
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
    hash.sort_by{|k, v| v }.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
    newer.each{ |item, quant| older[item] = quant }    
    older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
    count = Hash.new(0)
    word.chars.each{|letter| count[letter.downcase] += 1}
    count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
    count = Hash.new(0)
    arr.each{ |el| count[el] += 1 }
    count.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
    odd_or_even = {even: 0, odd: 0}
    numbers.each do |num|
      if num.even? == true
        odd_or_even[:even] += 1
      else
        odd_or_even[:odd] += 1
      end
    end
    odd_or_even
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
    string.delete!(",.:;?()")
    vowel_count = Hash.new(0)
    vowels = string.chars.select{|letter| letter.downcase if ("aeiou").include?(letter.downcase)}
    vowels.each{|vowel| vowel_count[vowel] += 1}
    vowel_count.sort_by{|k, v| v}.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
    students_fw = students.select{|name, month| month >= 7}
    names = students_fw.keys
    combinations = []
    names.each_index do |idx1|
        ((idx1 + 1)...names.length).each do |idx2|
        combinations << [names[idx1], names[idx2]]
      end
    end
    combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
    animals = Hash.new(0)
    specimens.each{|animal| animals[animal] += 1}
    bioindex = (animals.length**2) * (animals.values.min / animals.values.max)
    bioindex
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
    sign_letters = character_count(normal_sign.downcase)
    vandal_letters = character_count(vandalized_sign.downcase)
    vandal_letters.all? do |letter, quant|
    sign_letters[letter] >= quant 
  end
end

def character_count(str)
    str.delete!(".,?;:()")
    letters = Hash.new(0)
    str.chars.each{|letter| letters[letter] += 1}
    letters
end
